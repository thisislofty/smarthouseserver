package DAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.JOptionPane;

import org.hibernate.Session;

import utils.HibernateUtil;
import entities.UserEntity;

public class DAOImpl implements IDAO
{

	@PersistenceContext(unitName = "avatarsPersistenceUnit")
	private EntityManager em;

	@Override
	public <E> E getById(Class<E> entityClass, long id)
	{
		Session session = null;
		Object user = null;
		try
		{
			session = HibernateUtil.getSessionFactory().openSession();
			user = (UserEntity) session.get(UserEntity.class, id);
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage(), "������ I/O",
					JOptionPane.OK_OPTION);
		}
		finally
		{
			if (session != null && session.isOpen())
			{
				session.close();
			}
		}
		return (E) user;
	}

	@Override
	public <E> void saveEntity(E targetEntity)
	{
		Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.persist(targetEntity);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "������ I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
	}

	@Override
	public <E> void updateEntity(E targetEntity)
	{
		em.merge(targetEntity);
	}

	@Override
	public <E> void deleteEntity(E targetEntity)
	{
		em.remove(targetEntity);
	}

}
