package DAO;


public class Factory
{

	private static Factory instance = null;
	private static IDAO DAO = null;

	public static synchronized Factory getInstance()
	{
		if (instance == null)
		{
			instance = new Factory();
		}
		return instance;
	}

	public IDAO getDAO()
	{
		if (DAO == null)
		{
			DAO = (IDAO) new DAOImpl();
		}
		return DAO;
	}
	
}