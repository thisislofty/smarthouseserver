package DAO;

public interface IDAO
{
	public <E> E getById(Class<E> entityClass, long id);
	
	public <E> void saveEntity(E targetEntity);
	
	public <E> void updateEntity(E targetEntity);
	
	public <E> void deleteEntity(E targetEntity);
}
