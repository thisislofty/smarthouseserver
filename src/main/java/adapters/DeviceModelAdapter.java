package adapters;

import java.util.ArrayList;
import java.util.Calendar;

import avatars.Attribute;
import avatars.Device;
import avatars.DeviceGroup;
import avatars.GlobalCache;

public class DeviceModelAdapter
{
	private long id;
	private String name;
	private String group;
	private int status;
	private String lastChanged;
	private ArrayList<Attribute> atributes;

	public DeviceModelAdapter(Device device)
	{
		this.id = device.getId();
		this.name = device.getName();
		this.group = new String();
		for (DeviceGroup group : GlobalCache.getDeviceGroups())
		{
			if (group.getDevicesId().contains(device.getId()))
			{
				this.group+=group.getName()+",";
			}
		}
		// TODO:fix this
		this.lastChanged = "1915-31-12 09:00:00";
		this.atributes = device.getAtributes();

	}

	public DeviceModelAdapter(Device device, byte permitionStats)
	{
		this.id = device.getId();
		this.name = device.getName();
		this.group = new String();
		for (DeviceGroup group : GlobalCache.getDeviceGroups())
		{
			if (group.getDevicesId().contains(device.getId()))
			{
				this.group+=group.getName()+",";
			}
		}
		this.status = permitionStats;
		// TODO:fix this
		this.lastChanged = "1915-31-12 09:00:00";
		this.atributes = device.getAtributes();

	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getGroup()
	{
		return group;
	}

	public void setGroup(String group)
	{
		this.group = group;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String getLastChanged()
	{
		return lastChanged;
	}

	public void setLastChanged(String lastChanged)
	{
		this.lastChanged = lastChanged;
	}

	public ArrayList<Attribute> getAtributes()
	{
		return atributes;
	}

	public void setAtributes(ArrayList<Attribute> atributes)
	{
		this.atributes = atributes;
	}

}
