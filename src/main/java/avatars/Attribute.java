package avatars;

import java.util.ArrayList;

import entities.AtributesEntity;
import entities.SliderEntity;

public class Attribute
{

	private String icon;

	private boolean toggle;

	private ArrayList<Slider> slider;
        
        public Attribute(){}
	public Attribute(AtributesEntity attributesEntity)
	{
		this.icon = attributesEntity.getImage();
		this.toggle = attributesEntity.isToggle();
		this.slider = new ArrayList<Slider>();
//		for (SliderEntity sliderEntity : attributesEntity.getSliders())
//		{
//			this.slider.add(new Slider(sliderEntity));
//		}
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	public boolean isToggle()
	{
		return toggle;
	}

	public void setToggle(boolean toggle)
	{
		this.toggle = toggle;
	}

	public ArrayList<Slider> getSlider()
	{
		return slider;
	}

	public void setSlider(ArrayList<Slider> slider)
	{
		this.slider = slider;
	}

}
