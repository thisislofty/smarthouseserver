package avatars;


import entities.AtributesEntity;
import entities.DeviceEntity;
import enums.DeviceType;
import java.util.ArrayList;

public class Device
{
	private long id;

	private String name;
        
        private DeviceType type;
        

	private ArrayList<Attribute> attributes;

        public Device(){
        attributes = new ArrayList<Attribute>();
        }
	public Device(int id, String name, ArrayList<String> groups,
			ArrayList<Attribute> attributes)
	{
		this.id = id;
		this.name = name;
		this.attributes = attributes;
	}

	public Device(DeviceEntity deviceEntity)
	{
		this.id = deviceEntity.getId();
		this.name = deviceEntity.getDeviceName();
		this.attributes = new ArrayList<Attribute>();
//		for (AtributesEntity atributeEntity : deviceEntity.getAtributes())
//		{
//			this.attributes.add(new Attribute(atributeEntity));
//		}
	}

	public Long getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public ArrayList<Attribute> getAtributes()
	{
		return attributes;
	}

	public void setAtributes(ArrayList<Attribute> attributes)
	{
		this.attributes = attributes;
	}

    public DeviceType getType() {
        return type;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

}
