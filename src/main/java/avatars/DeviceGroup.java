package avatars;

import java.util.ArrayList;
import java.util.HashSet;

import com.google.gson.annotations.Expose;

public class DeviceGroup
{
	private String name;

	private String title;

	private String description;

	private String icon;

	private transient HashSet<Long> devicesId;

	public DeviceGroup()
	{
	}

	public DeviceGroup(String name, String title, String description,
			String icon, HashSet<Long> devicesId)
	{
		this.name = name;
		this.description = description;
		this.icon = icon;
		this.devicesId = devicesId;
		this.title = title;
	}

	public boolean addDevice(Device device)// return true, if succes
	{
		return this.devicesId.add(device.getId());
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	public HashSet<Long> getDevicesId()
	{
		return devicesId;
	}

	public void setDevicesId(HashSet<Long> devicesId)
	{
		this.devicesId = devicesId;
	}

	public String getTittle()
	{
		return title;
	}

	public void setTittle(String title)
	{
		this.title = title;
	}

}
