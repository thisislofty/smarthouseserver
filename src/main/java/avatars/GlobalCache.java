package avatars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class GlobalCache
{
	private static HashMap<String,User> users = new HashMap<String,User>();

	private static ArrayList<Device> devices = new ArrayList<Device>();

	private static ArrayList<DeviceGroup> deviceGroups = new ArrayList<DeviceGroup>();

	private static HashSet<UserGroup> userGroups = new HashSet<UserGroup>();

	

	public static void addUser(User user)
	{
		users.put(user.getUserLogin(),user);
	}

	public static HashMap<String,User> getUsers()
	{
		return users;
	}

	public static void setUsers(HashMap<String,User> users)
	{
		GlobalCache.users = users;
	}

	public static void addDevice(Device currentDevice)
	{
           boolean update = false;
            for(Device device: devices){
                if(device.getId()==currentDevice.getId())
                {
                    device = currentDevice;
                    update = true;
                }
            }
            if(!update){
		devices.add(currentDevice);
            }
	}

	public static ArrayList<Device> getDevices()
	{
		return devices;
	}

	public static void setDevices(ArrayList<Device> devices)
	{
		GlobalCache.devices = devices;
	}

	public static boolean addGroup(DeviceGroup group)
	{
		return deviceGroups.add(group);
	}

	public static ArrayList<DeviceGroup> getDeviceGroups()
	{
		return deviceGroups;
	}

	public static void setDeviceGroups(ArrayList<DeviceGroup> deviceGroups)
	{
		GlobalCache.deviceGroups = deviceGroups;
	}

	public static HashSet<UserGroup> getUserGroups()
	{
		return userGroups;
	}

	public static void setUserGroups(HashSet<UserGroup> userGroups)
	{
		GlobalCache.userGroups = userGroups;
	}

	public static boolean addUserGroup(UserGroup userGroup)
	{
		return userGroups.add(userGroup);
	}

    public static void addDeviceToGroups(Device activeDevice, String[] groups) {
       for(DeviceGroup group : deviceGroups)
       {
    	   for(String groupName : groups)
    	   {
    		   if(group.getName().equals(groupName))
    		   {
    			   group.getDevicesId().add(activeDevice.getId());
    		   }
    	   }
       }
    }

	public static void addDeviceGroup(DeviceGroup activeGroup)
	{
		deviceGroups.add(activeGroup);
		
	}

	public static DeviceGroup getDeviceGroupByName(String deviceGroupName)
	{
		DeviceGroup result = null;
		for (DeviceGroup deviceGroup: deviceGroups)
		{
			if(deviceGroup.getName().equals(deviceGroupName))
			{
				result = deviceGroup;
			}
		}
		return result;
	}

	public static void deleteDeviceGroup(String groupName)
	{
		for(DeviceGroup deviceGroup : deviceGroups)
		{
			if(deviceGroup.getName().equals(groupName))
			{
				deviceGroups.remove(deviceGroup);
			}
		}
	}

}
