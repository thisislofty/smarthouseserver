package avatars;

import entities.SliderEntity;

public class Slider {
	private String title;
	private int minValue;
	private int maxValue;
	private int value;
	
	public Slider(SliderEntity sliderEntity){
		this.title = sliderEntity.getTitle();
		this.minValue = sliderEntity.getMinValue();
		this.maxValue = sliderEntity.getMaxValue();
		this.value = sliderEntity.getValue();
	}
        
        public Slider(String name, int minValue, int maxValue){
		this.title = name;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
        
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getMinValue() {
		return minValue;
	}
	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}
	public int getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
}
