package avatars;

import java.util.ArrayList;
import java.util.HashMap;

public class UserGroup
{
	private String name;

	private HashMap<Long, Byte> devicePermition;

	private ArrayList<User> users;

	public UserGroup()
	{
		super();
	}

	public UserGroup(String name, HashMap<Long, Byte> devicePermition,
			ArrayList<User> users)
	{
		super();
		this.name = name;
		this.devicePermition = devicePermition;
		this.users = users;
		for (User user : users)
		{
			user.setUserGroup(this);
		}
	}

	public UserGroup(String name, HashMap<Long, Byte> devicePermition)
	{
		super();
		this.name = name;
		this.devicePermition = devicePermition;
		this.users = new ArrayList<User>();
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public HashMap<Long, Byte> getDevicePermition()
	{
		return devicePermition;
	}

	public void setDevicePermition(HashMap<Long, Byte> devicePermition)
	{
		this.devicePermition = devicePermition;
	}

	public ArrayList<User> getUsers()
	{
		return users;
	}

	public void setUsers(ArrayList<User> users)
	{
		this.users = users;
	}

}
