package controllers;

import java.util.ArrayList;

import adapters.DeviceModelAdapter;
import adapters.DevicesAdapter;
import avatars.Device;
import avatars.GlobalCache;
import avatars.User;

public class AdaptersController
{
	public static DevicesAdapter getDevicesAdapter(User user)
			throws Throwable
	{
		DevicesAdapter devicesAdapter;
		ArrayList<DeviceModelAdapter> deviceModelAdapters = new ArrayList<DeviceModelAdapter>();

		for (Device device : GlobalCache.getDevices())
		{
			Byte permition = (Byte) user.getUserGroup().getDevicePermition().get(
					device.getId());
			if (permition != null && permition > 0)
			{
				deviceModelAdapters.add(new DeviceModelAdapter(device,
						(byte) permition));
			}
		}
		devicesAdapter = new DevicesAdapter(deviceModelAdapters);
		return devicesAdapter;

	}

}
