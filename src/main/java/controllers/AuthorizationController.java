package controllers;

import avatars.User;

public class AuthorizationController
{
	public static boolean login(String userLogin, String password) throws Throwable
	{
		boolean result = false;
		User targetUser = AvatarsController.getUserByLogin(userLogin);
		
		if(targetUser!=null){
			result = targetUser.getPassword().equals(password);
		}
		
		return result;
	}

}
