package controllers;

import avatars.Device;
import avatars.GlobalCache;
import avatars.User;

public class AvatarsController
{
	public static User getUserByLogin(String userLogin) throws Throwable
	{
		User result = null;

		if ((result = GlobalCache.getUsers().get(userLogin)) == null)
		{
			throw new IllegalArgumentException();
		}
		return result;
	}

	public static Device getDeviceById(long id)
	{
		Device result = null;
		for (Device device : GlobalCache.getDevices())
		{
			if (device.getId() == id)
			{
				result = device;
				break;
			}
		}
		return result;
	}
}
