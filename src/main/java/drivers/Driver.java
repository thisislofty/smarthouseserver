package drivers;

import java.util.ArrayList;

import avatars.Device;

public interface Driver
{
	public abstract ArrayList<Device> getDevices();
	
	public abstract Device getDeviceState(Long deviceId);
	
	public abstract boolean setDeviceState(Device device);
	
}
