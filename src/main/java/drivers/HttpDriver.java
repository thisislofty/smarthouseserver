package drivers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import avatars.Device;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class HttpDriver implements Driver
{

	@Override
	public ArrayList<Device> getDevices()
	{
		ArrayList<Device> devices;
		String result = "";

		String url = "192.168.0.222:8080/getDevices";
		try
		{
			result = performRequest(new URL(url));
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}

		Type typeOfObjectsList = new TypeToken<ArrayList<Device>>() {}
				.getType();
		devices = new Gson().fromJson(result, typeOfObjectsList);

		return devices;
	}

	@Override
	public Device getDeviceState(Long deviceId)
	{
		Device device = null;

		String url = "192.168.0.222:8080/device/" + deviceId;

		try
		{
			device = new Gson().fromJson(performRequest(new URL(url)),
					Device.class);
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}

		return device;
	}

	@Override
	public boolean setDeviceState(Device device)
	{
		boolean success = false;
		ArrayList<String> requests = new ArrayList<>();

		Device devicesPresentState = getDeviceState(device.getId());

		if (!device.getName().equals(devicesPresentState.getName()))
		{
			requests.add("192.168.0.222:8080/set/" + device.getId() + "/name/"
					+ device.getName());
		}
		for (int i = 0; i < device.getAtributes().size(); i++)
		{
			if (device.getAtributes().get(i).isToggle() != devicesPresentState
					.getAtributes().get(i).isToggle())
			{
				requests.add("192.168.0.222:8080/set/" + device.getId()
						+ "/attribute/" + i + "/"
						+ device.getAtributes().get(i).isToggle());
			}
			for (int j = 0; j < device.getAtributes().get(i).getSlider().size(); j++)
			{
				if (device
						.getAtributes()
						.get(i)
						.getSlider()
						.get(j)
						.getTitle()
						.equals(devicesPresentState.getAtributes().get(i)
								.getSlider().get(j).getTitle()))
				{
					requests.add("192.168.0.222:8080/set/"
							+ device.getId()
							+ "/attribute/"
							+ i
							+ "/slider/"
							+ j
							+ "/tittle/"
							+ devicesPresentState.getAtributes().get(i)
									.getSlider().get(j).getTitle());
				}
				if (device.getAtributes().get(i).getSlider().get(j).getValue() == devicesPresentState
						.getAtributes().get(i).getSlider().get(j).getValue())
				{
					requests.add("192.168.0.222:8080/set/"
							+ device.getId()
							+ "/attribute/"
							+ i
							+ "/slider/"
							+ j
							+ "/value/"
							+ devicesPresentState.getAtributes().get(i)
									.getSlider().get(j).getValue());
				}
			}
		}
		for (String request : requests)
		{
			try
			{
				performRequest(new URL(request));
				success = true;
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return success;
	}

	private String performRequest(URL url)
	{
		HttpURLConnection conn;
		BufferedReader rd;
		String line;
		String result = "";
		try
		{
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			rd = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			while ((line = rd.readLine()) != null)
			{
				result += line;
			}
			rd.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
}
