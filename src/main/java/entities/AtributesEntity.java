package entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "atributes")
public class AtributesEntity
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	 private Long id;
	
	@ManyToOne()
	@JoinColumn(name = "deviceid")
	 private DeviceEntity device;

	@Column(name = "image")
	private String image;
	
	@Column(name = "toggle")
	private boolean toggle;
	
	@OneToMany(mappedBy = "atribute")
	private List<SliderEntity> sliders;
	
	

	public AtributesEntity()
	{
	}

	public AtributesEntity(Long id, DeviceEntity device, String image,
			boolean toggle, List<SliderEntity> sliders)
	{
		this.id = id;
		this.device = device;
		this.image = image;
		this.toggle = toggle;
		this.sliders = sliders;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public DeviceEntity getDevice()
	{
		return device;
	}

	public void setDevice(DeviceEntity device)
	{
		this.device = device;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage(String image)
	{
		this.image = image;
	}

	public boolean isToggle()
	{
		return toggle;
	}

	public void setToggle(boolean toggle)
	{
		this.toggle = toggle;
	}

	public List<SliderEntity> getSliders()
	{
		return sliders;
	}

	public void setSliders(List<SliderEntity> sliders)
	{
		this.sliders = sliders;
	}
	

	
}
