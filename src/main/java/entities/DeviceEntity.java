package entities;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "devicesentity")
public class DeviceEntity
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String groups; // as JSON

	@Column(name = "device_name")
	private String deviceName;

	@Column
	@OneToMany(targetEntity = AtributesEntity.class, mappedBy = "device", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<AtributesEntity> atributes;

	public DeviceEntity()
	{
	}

	public DeviceEntity(long id, String deviceName, String groups,
			Set<AtributesEntity> atributes)
	{
		this.id = id;
		this.deviceName = deviceName;
		this.groups = groups;
		this.atributes = atributes;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}

	public String getGroups()
	{
		return groups;
	}

	public void setGroups(String groups)
	{
		this.groups = groups;
	}

	public Set<AtributesEntity> getAtributes()
	{
		return atributes;
	}

	public void setAtributes(Set<AtributesEntity> atributes)
	{
		this.atributes = atributes;
	}

}
