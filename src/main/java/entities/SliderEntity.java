package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sliders")
public class SliderEntity
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "title")
	private String title;

	@Column(name = "value")
	private int value;

	@Column(name = "max_value")
	private int maxValue;

	@Column(name = "min_value")
	private int minValue;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atribute")
	AtributesEntity atribute;

	
	public SliderEntity()
	{
		super();
	}

	public SliderEntity(long id, String title, int value, int maxValue,
			int minValue, AtributesEntity atribute)
	{
		super();
		this.id = id;
		this.title = title;
		this.value = value;
		this.maxValue = maxValue;
		this.minValue = minValue;
		this.atribute = atribute;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public int getValue()
	{
		return value;
	}

	public void setValue(int value)
	{
		this.value = value;
	}

	public int getMaxValue()
	{
		return maxValue;
	}

	public void setMaxValue(int maxValue)
	{
		this.maxValue = maxValue;
	}

	public int getMinValue()
	{
		return minValue;
	}

	public void setMinValue(int minValue)
	{
		this.minValue = minValue;
	}

	public AtributesEntity getAtribute()
	{
		return atribute;
	}

	public void setAtribute(AtributesEntity atribute)
	{
		this.atribute = atribute;
	}
	
	
}
