package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "userentity")
public class UserEntity
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(unique = true)
	private String login; 
	
	private String password;

	private String name;

	@ManyToOne
	@JoinColumn(name="groupId")
	private UserGroupEntity group;

	
	public UserEntity()
	{
	}

	public UserEntity(Long id, String login, String password, String name,
			UserGroupEntity group)
	{
		this.id = id;
		this.login = login;
		this.password = password;
		this.name = name;
		this.group = group;
	}



	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}



	public UserGroupEntity getGroup()
	{
		return group;
	}



	public void setGroup(UserGroupEntity group)
	{
		this.group = group;
	}



	public String getLogin()
	{
		return login;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	@Override
	public String toString()
	{
		return "UserEntity [id=" + id + ", login=" + login + ", password="
				+ password + ", name=" + name + ", group=" + group + "]";
	}



}
