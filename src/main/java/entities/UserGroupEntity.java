package entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "usergroup")
public class UserGroupEntity
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	private String devicePermition;
	
	@OneToMany(mappedBy="group")
	private Set<UserEntity> users;
	
	

	public UserGroupEntity()
	{
	}

	public UserGroupEntity(Long id, String name, String devicePermition,
			Set<UserEntity> users)
	{
		super();
		this.id = id;
		this.name = name;
		this.devicePermition = devicePermition;
		this.users = users;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDevicePermition()
	{
		return devicePermition;
	}

	public void setDevicePermition(String devicePermition)
	{
		this.devicePermition = devicePermition;
	}

	public Set<UserEntity> getUsers()
	{
		return users;
	}

	public void setUsers(Set<UserEntity> users)
	{
		this.users = users;
	}
	
	
}
