package enums;

public enum Permission {
	Unvisible,
	Visible,
	Editable;
}
