package handlers;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import DAO.Factory;
import entities.UserEntity;


public class AddUserHandler extends AbstractHandler
{

	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException
	{
		response.setContentType("text/html;charset=utf-8");
	

		String username = request.getParameter("name");
		String role = request.getParameter("role");

		
		
		response.setStatus(HttpServletResponse.SC_OK);
		response.setHeader("Access-Control-Allow-Origin", "*");
		baseRequest.setHandled(true);
		response.getWriter().println("User "+username+" added with role "+role);

	}

}
