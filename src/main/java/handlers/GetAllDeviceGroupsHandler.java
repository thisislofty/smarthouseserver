package handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import avatars.GlobalCache;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GetAllDeviceGroupsHandler extends AbstractHandler
{

	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException
	{

		boolean succes = false;
		response.setContentType("application/json;charset=utf-8");
		if (request.getMethod().equals("OPTIONS"))
		{
			System.out.println("333" + request.getMethod());
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods",
					"GET, POST, DELETE, PUT");
			response.setHeader(
					"Access-Control-Allow-Headers",
					"X-Requested-With, Content-Type, X-Codingpedia, X-CH-access, Access-Control-Allow-Origin");
			response.setStatus(HttpServletResponse.SC_OK);
			baseRequest.setHandled(true);
		}
		else
		{

			try
			{
				Gson gson = new GsonBuilder().disableHtmlEscaping()
						.setPrettyPrinting().serializeNulls().create();
				response.getWriter().println(
						gson.toJson(GlobalCache.getDeviceGroups()));

				succes = true;
			}
			catch (Throwable e)
			{
				e.printStackTrace();
			}

			baseRequest.setHandled(true);
			response.setStatus(succes ? HttpServletResponse.SC_OK
					: HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods",
					"GET, POST, DELETE, PUT");
			response.setHeader(
					"Access-Control-Allow-Headers",
					"X-Requested-With, Content-Type, X-Codingpedia, X-CH-access, Access-Control-Allow-Origin");
		}
	}

}
