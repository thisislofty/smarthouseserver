package handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import adapters.DevicesAdapter;
import avatars.User;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import controllers.AdaptersController;
import controllers.AuthorizationController;
import controllers.AvatarsController;

public class GetUserDevicesHandler extends AbstractHandler
{
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException
	{
		int status;
		if (request.getMethod().equals("OPTIONS"))
		{
			System.out.println("11111" + request.getMethod());
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods",
					"GET, POST, DELETE, PUT");
			response.setHeader(
					"Access-Control-Allow-Headers",
					"X-Requested-With, Content-Type, X-Codingpedia, X-CH-access, Access-Control-Allow-Origin");
			status = HttpServletResponse.SC_OK;
			response.setStatus(status);
			baseRequest.setHandled(true);
		}
		else
		{
			System.out.println("22222" + request.getMethod());
			String userLogin = request.getParameter("user");
			String password = request.getHeader("X-CH-access");

			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

			System.out.println("Request for devices for user " + userLogin
					+ "password " + password);
			
			User user;
			try
			{
				user = AvatarsController.getUserByLogin("admin");
		

			DevicesAdapter deviceAdapter = AdaptersController
					.getDevicesAdapter(user);
			Gson gson = new GsonBuilder().disableHtmlEscaping()
					.setPrettyPrinting().serializeNulls().create();
			response.getWriter().println(gson.toJson(deviceAdapter));
			status = HttpServletResponse.SC_OK;
			response.setContentType("application/json;charset=utf-8");
			}
			catch (Throwable e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try
			{
				if (AuthorizationController.login(userLogin, password))
				{
//					User user = AvatarsController.getUserByLogin("admin");
//
//					DevicesAdapter deviceAdapter = AdaptersController
//							.getDevicesAdapter(user);
//					Gson gson = new GsonBuilder().disableHtmlEscaping()
//							.setPrettyPrinting().serializeNulls().create();
//					response.getWriter().println(gson.toJson(deviceAdapter));

					status = HttpServletResponse.SC_OK;
				}
				else
				{
					status = HttpServletResponse.SC_NOT_FOUND;
					// status = HttpServletResponse.SC_OK;
				}
			}
			catch (Throwable e)
			{
				e.printStackTrace();
			}
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods",
					"GET, POST, DELETE, PUT");
			response.setHeader(
					"Access-Control-Allow-Headers",
					"X-Requested-With, Content-Type, X-Codingpedia, X-CH-access, Access-Control-Allow-Origin");

			baseRequest.setHandled(true);
			response.setStatus(status);

		}
	}
}
