package handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import controllers.AuthorizationController;

public class LoginHandler extends AbstractHandler
{
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException
	{
		
		boolean succes = false;
		response.setContentType("application/json;charset=utf-8");
		System.out.println("login"+request.getMethod());
		if (request.getMethod().equals("POST"))
		{
			try
			{
				System.out.println(request.getParameterMap().get("user"));
				String user = request.getParameter("user");
				String password = request.getParameter("password");
				
				System.out.println("Userlogin "+user+" with pswd "+password);

				succes = AuthorizationController.login(user, password);
				if(succes){
					response.getWriter().println("{\"status\":\"ok\"}");
				}
			}
			catch (Throwable e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("Wrong request type for login request");
		}
		baseRequest.setHandled(true);
		response.setStatus(succes ? HttpServletResponse.SC_OK
				: HttpServletResponse.SC_FORBIDDEN);
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods",
				"GET, POST, DELETE, PUT");
		response.setHeader("Access-Control-Allow-Headers",
				"X-Requested-With, Content-Type, X-Codingpedia");
	}

}
