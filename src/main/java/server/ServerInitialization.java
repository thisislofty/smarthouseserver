package server;

import handlers.AddUserHandler;
import handlers.GetAllDeviceGroupsHandler;
import handlers.GetUserDevicesHandler;
import handlers.LoginHandler;
import handlers.TestHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;

import avatars.Device;
import avatars.DeviceGroup;
import avatars.GlobalCache;
import avatars.User;
import avatars.UserGroup;
import entities.AtributesEntity;
import entities.DeviceEntity;
import entities.SliderEntity;

public class ServerInitialization
{
	public static Server initServer(int portNumber) throws Exception
	{
		createDummyDevice();
		createDummyUser();
		createDummyUserGroup();
		createDummyGroup();

		Server server = new Server(portNumber);
		ContextHandler context = new ContextHandler();
		context.setContextPath("/adduser");
		context.setHandler(new AddUserHandler());
		
		ContextHandler test = new ContextHandler();
		context.setContextPath("/test");
		context.setHandler(new TestHandler());

		ContextHandler context2 = new ContextHandler();
		context2.setAllowNullPathInfo(true);
		context2.setContextPath("/devices");
		context2.setHandler(new GetUserDevicesHandler());

		ContextHandler getGroupContext = new ContextHandler();
		getGroupContext.setAllowNullPathInfo(true);
		getGroupContext.setContextPath("/device_groups");
		getGroupContext.setHandler(new GetAllDeviceGroupsHandler());
		
		ContextHandler loginContext = new ContextHandler();
		loginContext.setAllowNullPathInfo(true);
		loginContext.setContextPath("/login");
		loginContext.setHandler(new LoginHandler());

		ContextHandlerCollection contextsCollection = new ContextHandlerCollection();
		contextsCollection.addHandler(context);
		contextsCollection.addHandler(context2);
		contextsCollection.addHandler(getGroupContext);
		contextsCollection.addHandler(loginContext);

		server.setHandler(contextsCollection);

		server.start();
		// server.join();
		return server;
	}

	private static void createDummyDevice()
	{
		DeviceEntity device = new DeviceEntity();
		device.setId(1L);
		device.setDeviceName("Лампочка");
		List<String> groups = new ArrayList<String>();
		groups.add("Kitchen");
		Set<AtributesEntity> atributes = new HashSet<AtributesEntity>();
		AtributesEntity atribute = new AtributesEntity();
		SliderEntity slider = new SliderEntity();
		slider.setTitle("Яркость");
		slider.setMinValue(0);
		slider.setMaxValue(10);
		slider.setValue(5);
		ArrayList<SliderEntity> sliders = new ArrayList<SliderEntity>();
		sliders.add(slider);
		atribute.setSliders(sliders);
		atribute.setImage("no image");
		atribute.setToggle(false);
		atributes.add(atribute);
		device.setAtributes(atributes);

		Device deviceModel = new Device(device);

		GlobalCache.addDevice(deviceModel);
	}

	private static void createDummyGroup()
	{
		HashSet<Long> devices = new HashSet<Long>();
		for (Device device : GlobalCache.getDevices())
		{
			devices.add(device.getId());
		}
		DeviceGroup deviceGroup = new DeviceGroup("Kitchen", "Кухня",
				"Приборы в кухне", "/someicon.png", devices);

		GlobalCache.addGroup(deviceGroup);
	}

	private static void createDummyUser()
	{
		User user = new User();
		user.setUserId(1L);
		user.setUserLogin("admin");
		user.setUserName("Administrator");
		//user.setPassword("202cb962ac59075b964b07152d234b70");
		user.setPassword("admin");
		HashMap<Long, Byte> devicePermition = new HashMap<Long, Byte>();
		devicePermition.put(1L, (byte) 1);
		GlobalCache.addUser(user);

	}

	private static void createDummyUserGroup()
	{
		HashMap<Long, Byte> devicePermition = new HashMap<Long, Byte>();
		devicePermition.put(1L, (byte) 2);
		ArrayList<User> users = new ArrayList<User>();
		users = new ArrayList<User>(GlobalCache.getUsers().values());
		GlobalCache
				.addUserGroup(new UserGroup("Admin", devicePermition, users));

	}

}